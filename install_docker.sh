#!/usr/bin/env bash
rm /var/lib/apt/lists/lock
rm /var/cache/apt/achives/lock
rm /var/lib/dpkg/lock

apt update
apt upgrade -y
apt install openssh-server -y
apt install apt-transport-https ca-certificates curl software-properties-common curl -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
apt update
apt install docker-ce -y
usermod -aG docker submerged

curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

mkdir /settings
