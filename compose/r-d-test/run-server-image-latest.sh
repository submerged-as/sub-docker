#!/usr/bin/env bash
IMAGENAME=record-data
docker pull larsmartinaas/${IMAGENAME}:latest
docker run -it --privileged -v /dev:/dev -v /data:/data -v /settings:/settings larsmartinaas/${IMAGENAME}:latest
