#!/usr/bin/env bash
IMAGENAME=uc-watchdog
docker pull larsmartinaas/${IMAGENAME}:latest
docker run -it --privileged -v /data:/data -v /settings:/settings -v /dev:/dev larsmartinaas/${IMAGENAME}:latest