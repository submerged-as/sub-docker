#!/usr/bin/env bash
IMAGENAME=uc-watchdog
docker pull larsmartinaas/${IMAGENAME}:develop
docker run -it --privileged -v /data:/data -v /settings:/settings -v /dev:/dev larsmartinaas/${IMAGENAME}:develop
