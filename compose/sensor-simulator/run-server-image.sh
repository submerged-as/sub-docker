#!/usr/bin/env bash
IMAGENAME=sensor-simulator
docker pull larsmartinaas/${IMAGENAME}:latest
docker run -it -v /data:/data -v /settings:/settings larsmartinaas/${IMAGENAME}:latest